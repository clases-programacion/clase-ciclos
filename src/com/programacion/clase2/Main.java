package com.programacion.clase2;

public class Main {
    /**
     * Probamos la funcion de las sentencias if, else if y else con una condición.
     */
    public static void puebaDeIfSimple() {
        System.out.println("\n\n\nEstamos en el método puebaDeIfSimple");
        String unStringCualquiera = "Esto es un ejemplo";
        if (unStringCualquiera.equals("Otro texto")) {
            //Se cumple esta confición si y solo si unString es igual al texto "Esto es un ejemplo"
            System.out.println("El programa nunca va a entrar aquí");
        } else if (unStringCualquiera.equals("Esto es un ejemplo")) {
            //Se cumple esta confición si y solo si unString es igual al texto "El programa va a entrar aquí"
            System.out.println("El programa va a entrar aquí");
        } else {
            System.out.println("En caso que no se cumpla ninguna condición entro aquí");
        }
    }

    /**
     * Probamos la funcion de las sentencias if, else if y else con mas de una condición.
     * Importante, pueden añadir tantas condiciones como quieran
     * Además asociar condiciones con paréntesis
     */
    public static void puebaDeIfMultiple() {
        System.out.println("\n\n\nEstamos en el método puebaDeIfMultiple");
        String unStringCualquiera = "Esto es un ejemplo";
        if (unStringCualquiera != null && unStringCualquiera.equals("Otro texto")) {
            /**
             * Se cumple esta confición si y solo si unString no es nulo y es igual al texto "Esto es un ejemplo"
             * Importante nulo es distinto de ""
             */
            System.out.println("El programa nunca va a entrar aquí");
        } else if (unStringCualquiera != null || unStringCualquiera.equals("Otro texto")) {
            //Se cumple esta confición si unStringCualquiera no es nulo o unStringCualquiera es igual al texto "Otro texto"
            System.out.println("Ya que unStringCualquiera no es nulo, el programa va a entrar aquí");
        } else {
            System.out.println("En caso que no se cumpla ninguna condición entro aquí");
        }
    }

    /**
     * Probamos la funcion de la sentencia switch.
     */
    public static void puebaDeSwitch() {
        System.out.println("\n\n\nEstamos en el método puebaDeSwitch");
        String unEnteroCualquiera = "uno";
        switch (unEnteroCualquiera) {
            case "uno":
                //Validamos que unEnteroCualquiera sea igual a uno
                System.out.println("El programa va a entrar aquí ya que unEnteroCualquiera es uno");
                break;
            case "dos":
                //Validamos que unEnteroCualquiera sea igual a dos
                System.out.println("El programa nunca  va a entrar aquí ya que unEnteroCualquiera no es dos");
                //Con break salimos del switch
                break;
            case "cero":
            case "tres":
            case "cuatro":
                //Validamos que unEnteroCualquiera sea igual a cero o tres o a cuatro
                System.out.println("El programa nunca  va a entrar aquí ya que unEnteroCualquiera no es cero o tres o a cuatro");
                //Con break salimos del switch
                break;
            default:
                //En caso de no cumplir alguna de las condiciones anteriores entraremos aquí
                System.out.println("En caso de no cumplir alguna de las condiciones anteriores entraremos aquí");
                //Con break salimos del switch
                break;
        }
    }

    /**
     * Probamos la funcion del ciclo for.
     */
    public static void puebaDeForClasico() {
        System.out.println("\n\n\nEstamos en el método puebaDeFor");
        for (int i = 0; i < 10; i++) {
            //Ejecutamos esto hasta que i sea mayor o igual a 10, además sumamos a i 1 cada que ejecutamos
            System.out.println("i = " + i);
        }
    }

    /**
     * Probamos la funcion del ciclo for con un array.
     */
    public static void puebaDeForEnArray() {
        System.out.println("\n\n\nEstamos en el método puebaDeForEnArray");
        String[] strings = {"valor1", "valor2", "valor3", "valor4", "valor5", "valor6",
                "valor7", "valor8", "valor9", "valor10"};
        System.out.println("Length=" + strings.length);
        for (int i = 0; i < strings.length; i++) {
            //Ejecutamos esto hasta que i sea mayor o igual al tamaño del array strings,
            // además sumamos a i 1 cada que ejecutamos
            System.out.println("strings[" + i + "] = " + strings[i]);
        }
    }

    /**
     * Probamos la funcion del ciclo for con un array.
     */
    public static void puebaDeForEnArraySimple() {
        System.out.println("\n\n\nEstamos en el método puebaDeForEnArraySimple");
        String[] strings = {"valor1", "valor2", "valor3", "valor4", "valor5", "valor6", "valor7", "valor8", "valor9", "valor10"};
        for (String unstring: strings) {
            //Ejecutamos esto para cada elemento de strings
            System.out.println(unstring);
        }
    }

    /**
     * Probamos la funcion del ciclo while.
     */
    public static void puebaDeWhile() {
        System.out.println("\n\n\nEstamos en el método puebaDeWhile");
        int i = 10;
        while(i > 0) {
            //Ejecutamos esto hasta que i sea menor o igual a 0
            System.out.println("i = " + i);
            //Restamos 1 a i, podemos usarlo también en el ciclo for
            i--;
        }
    }

    /**
     * Probamos la funcion del ciclo while. NO EJECUTAR ES UN CICLO INFINITO
     */
    public static void puebaDeWhileInfinito() {
        System.out.println("\n\n\nEstamos en el método puebaDeWhileInfinito");
        int i = 10;
        while(true) {
            System.out.println("i = " + i);
            //Restamos 1 a i, podemos usarlo también en el ciclo for
            i--;
        }
    }

    /**
     * Probamos la funcion del ciclo do while
     */
    public static void puebaDeDoWhile() {
        System.out.println("\n\n\nEstamos en el método puebaDeDoWhile");
        int i = -10;
        do {
            //Ejecutamos una vez, después ejecutamos hasta que i sea menor o igual a 0
            System.out.println("i = " + i);
            //Restamos 1 a i, podemos usarlo también en el ciclo for
            i--;
        } while(i > 0);
    }

    public static void main(String[] args) {
        System.out.println("Hola este es el método main :)");
        /**
         * Las sentencias if, else if, else, switch verifican si se cumplen la(s) condición(es)
         */
        //Ejecutamos el método puebaDeIfSimple()
        puebaDeIfSimple();
        /**
         * Ya que el método puebaDeIfMultiple es static podemos ejecutar la funcion
         * sin instanciar el objeto Main
         * En otras palabras no es necesario el Main main = new Main();
         * Ejecutamos el método puebaDeIfMultiple
         */
        Main.puebaDeIfMultiple();
        /**
         * Ya que el método puebaDeSwitch es static no podemos
         * ejecutar al método puebaDeSwitch con this
         * Es decir no podemos hacer this.puebaDeSwitch()
         * Ejecutamos el métod puebaDeSwitch
         */
        puebaDeSwitch();



        /**
         * Los ciclos ejecutan lo definido hasta que se cumple una condición
         * El ciclo for y while pueden parecer similares
         * Sin embargo con el ciclo while no sabemos cuando va a terminar
         */
        //Ejecutamos el método puebaDeForClasico
        puebaDeForClasico();
        //Ejecutamos el método puebaDeForEnArray
        puebaDeForEnArray();
        //Ejecutamos el método puebaDeForEnArraySimple
        puebaDeForEnArraySimple();
        //Ejecutamos el método puebaDeWhile
        puebaDeWhile();
        //Ejecutamos el método puebaDeDoWhile
        puebaDeDoWhile();
    }
}
